package JSON::API::v1::Roles::TO_JSON;
our $VERSION = '0.003';
use Moose::Role;
use namespace::autoclean;

# ABSTRACT: An interface for Objects to adhere to

requires qw(
    TO_JSON
);


1;

__END__

=head1 DESCRIPTION

This role implements an interface to which consumers must adhere to. It defines
several methods that L<JSON::API::v1> namespaced objects must implement
to support the JSON API v1 specifications.

=head1 SYNOPSIS

=head1 AUTHOR

Wesley Schwengle

=head1 LICENSE and COPYRIGHT

Wesley Schwengle, 2017.

