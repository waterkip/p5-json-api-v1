package JSON::API::v1::Roles::Links;
our $VERSION = '0.003';
use Moose::Role;
use namespace::autoclean;

# ABSTRACT: An role that implements the default links object

has links => (
    is        => 'ro',
    isa       => 'JSON::API::v1::Links',
    predicate => 'has_links',
);

1;

__END__

=head1 DESCRIPTION

This role makes sure that you never have to implement a links attributes.

=head1 SYNOPSIS
