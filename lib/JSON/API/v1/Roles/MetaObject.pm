package JSON::API::v1::Roles::MetaObject;
our $VERSION = '0.003';
use Moose::Role;
use namespace::autoclean;

# ABSTRACT: An role that implements the default meta object

has meta_object => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_meta_object',
    init_arg  => 'meta',
);

1;

__END__

=head1 DESCRIPTION

This role makes sure that you never have to implement a meta object. Added
because L<Moose> already has a C<meta> function, so we need to rename it a bit.


=head1 SYNOPSIS
