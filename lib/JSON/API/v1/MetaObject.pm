use utf8;

package JSON::API::v1::MetaObject;
our $VERSION = '0.003';
use Moose;
use namespace::autoclean;

# ABSTRACT: A JSON API Meta object according to jsonapi v1 specification

has members => (
    is      => 'ro',
    isa     => 'HashRef',
    traits  => ['Hash'],
    lazy    => 1,
    default => sub { {} },
    handles => {
        has_member       => 'exists',
        get_member       => 'get',
        set_member       => 'set',
        add_member       => 'set',
        clear_member     => 'delete',
        get_member_names => 'keys',
    },
);

sub TO_JSON {
    my $self = shift;

    my %rv;
    foreach ($self->get_member_names) {
        $rv{$_} = $self->get_member($_);
    }
    return \%rv;
}

with qw(
    JSON::API::v1::Roles::TO_JSON
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This module attempts to make a Moose object behave like a JSON API object as
defined by L<jsonapi.org>. This object adheres to the v1 specification

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 METHODS
